package com.desafioconcrete.android.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertDialogManager {

	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		
		/*if (status != null){
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		}*/
		
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int which) {
				// TODO Auto-generated method stub	
			}
		});
		
		alertDialog.show();
	}
	
	public AlertDialogManager() {
		// TODO Auto-generated constructor stub
	}

}
