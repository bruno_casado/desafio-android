package com.desafioconcrete.android.REST;


public class REST{

	private static final String URL = "http://correiosapi.apphb.com/cep/";
	
	public static String getCEP(String cep) throws Exception{
		String[] resposta = new WebServiceGet().doInBackground(URL + cep);
		
		if (resposta[0].equals("200")){
			return resposta[1];
		} else if (resposta[0].equals("404")){
			return "404";
		} else if (resposta[0].equals("400")){
			return "400";
		} else{
			throw new Exception(resposta[1]);
		}
	}
}
