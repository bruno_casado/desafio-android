package com.desafioconcrete.android.REST;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;


public class HttpClientSingleton {

	private static final int JSON_CONNECTION_TIMEOUT = 10000;
	private static final int JSON_SOCKET_TIMEOUT = 12000;
	private static HttpClientSingleton instance;
	private HttpParams httpParams;
	private DefaultHttpClient httpClient;
	
	private void setTimeOut(HttpParams params){
		HttpConnectionParams.setConnectionTimeout(params, JSON_CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(params, JSON_SOCKET_TIMEOUT);
	}
	
	private HttpClientSingleton() {
		httpParams = new BasicHttpParams();
		setTimeOut(httpParams);
		httpClient = new DefaultHttpClient(httpParams);
	}

	public static DefaultHttpClient getHttpClientInstance(){
		Log.e("Singleton", "Default");
		if (instance == null){
			Log.e("Singleton", "Null");
			instance = new HttpClientSingleton();
		}
		return instance.httpClient;
	}
}
