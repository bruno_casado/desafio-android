package com.desafioconcrete.android;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desafioconcrete.android.Model.Endereco;
import com.desafioconcrete.android.REST.REST;
import com.desafioconcrete.android.util.AlertDialogManager;
import com.desafioconcrete.android.util.ConnectionDetector;
import com.desafioconcrete.android.util.ExibirConsultasDialogFragment;
import com.desafioconcrete.android.util.JsonSharedPreferences;
import com.desafioconcrete.android.util.Mascara;
import com.google.gson.Gson;


@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends Activity {

	Endereco endereco;
	EditText cep;
	Button buscarButton;
	Button consultarButton;
	LinearLayout resultadoLayout;
	TextView resultadoTextView;
	String resultado;
	AlertDialogManager alert = new AlertDialogManager();
	Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		cep = (EditText) findViewById(R.id.cepEditText);
		cep.addTextChangedListener(Mascara.insert("#####-###", cep));
		
		buscarButton = (Button) findViewById(R.id.buscarButton);
		consultarButton = (Button) findViewById(R.id.consultarButton);
		
		resultadoLayout = (LinearLayout) findViewById(R.id.resultadoLayout);
		resultadoTextView = (TextView) findViewById(R.id.resultadoTextView);
		
		
		buscarButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (cep.getText().length() < 9){
					alert.showAlertDialog(MainActivity.this, "Erro!", "O CEP deve conter 8 n�meros");
				} else{
					NetAsync(v);					
				}
			}
		});
		
		consultarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				DialogFragment newFragment = new ExibirConsultasDialogFragment();
				newFragment.show(getFragmentManager(), "consultas");
			}
		});
		
		Drawable background = getResources().getDrawable(R.drawable.background);
		Drawable background_land = getResources().getDrawable(R.drawable.background_land);
		background.setAlpha(40);
		background_land.setAlpha(40);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putString("endereco", resultadoTextView.getText().toString());
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState){
		super.onRestoreInstanceState(savedInstanceState);
		resultadoTextView.setText(savedInstanceState.getString("endereco"));
		resultadoTextView.setTextColor(Color.BLUE);
		resultadoLayout.setVisibility(0);
	}
	
	public void NetAsync(View view){
    	new NetCheck().execute();
    }
	
	private class NetCheck extends AsyncTask<Object, Object, Boolean>{

    	private ProgressDialog nDialog;
    	
    	@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		
    		nDialog = new ProgressDialog(MainActivity.this);
    		nDialog.setTitle("Verificando rede");
    		nDialog.setMessage("Carregando...");
    		nDialog.setIndeterminate(false);
    		nDialog.setCancelable(true);
    		nDialog.show();
    	}
    	
		@Override
		protected Boolean doInBackground(Object... params) {
			// TODO Auto-generated method stub
			ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
			return cd.isConnectingToInternet();
		}
		
		protected void onPostExecute(Boolean th){
			if (th == true){
				nDialog.dismiss();
				new ProcessSearch().execute();
			} else{
				nDialog.dismiss();
				alert.showAlertDialog(MainActivity.this, "Erro de conex�o!", "Conex�o n�o dispon�vel!");
			}
		}
    }
	
	private class ProcessSearch extends AsyncTask<Object, Object, Endereco>{
    	
    	private ProgressDialog pDialog;
    	Endereco endereco;
    	
    	@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		
    		pDialog = new ProgressDialog(MainActivity.this);
    		pDialog.setTitle("Conectando ao Servidor");
    		pDialog.setMessage("Autenticado...");
    		pDialog.setIndeterminate(false);
    		pDialog.setCancelable(true);
    		pDialog.show();
    	}

		@Override
		protected Endereco doInBackground(Object... params) {
			// TODO Auto-generated method stub
			try {
				resultado = REST.getCEP(Mascara.unmask(cep.getText().toString()));
				if (resultado.equals("400")){
					return null;
				} else if (resultado.equals("404")){
					return null;
				} else{
					Gson gson = new Gson();
					endereco = gson.fromJson(resultado, Endereco.class);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return endereco;
		}
    	
		protected void onPostExecute(Endereco endereco){
			if (endereco != null){
				pDialog.setMessage("Carregando o dados da pesquisa");
				pDialog.setTitle("Adquirindo dados");
				resultadoLayout.setVisibility(0);
				JSONArray jsonArray = new JSONArray();
				try {
					JSONObject teste = new JSONObject();
					String s = "";
					teste.put("tipoDeLogradouro", endereco.getTipo());
					teste.put("logradouro", endereco.getLogradouro());
					teste.put("bairro", endereco.getBairro());
					teste.put("cidade", endereco.getCidade());
					teste.put("estado", endereco.getEstado());
					teste.put("cep", endereco.getCep());
					s = teste.toString();
					Log.e("Main", "JSON.ToString(): " + s);
					jsonArray = JsonSharedPreferences.loadJSONArray(getApplicationContext(), "Enderecos", "cep");
					jsonArray.put(teste);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				JsonSharedPreferences.saveJSONArray(getApplicationContext(), "Enderecos", "cep", jsonArray);
				resultadoTextView.setText(endereco.getTipo() + " " +endereco.getLogradouro() + ".\n" + "Bairro: " + endereco.getBairro() + 
		        		".\n" + endereco.getCidade() + " / " + endereco.getEstado() + "\n" + "CEP: " + endereco.getCep());
				resultadoTextView.setTextColor(Color.BLUE);

				pDialog.dismiss();
			} else{
				pDialog.dismiss();
				if (resultado.equals("400")){
					alert.showAlertDialog(MainActivity.this, "Erro!", "Servi�o Indispon�vel!");
				} else if (resultado.equals("404")){
					alert.showAlertDialog(MainActivity.this, "Erro!", "CEP inv�lido!");
				}
				
			}
		}
    }

}
