package com.desafioconcrete.android.Model;

import java.io.Serializable;

public class Endereco implements Serializable{

	private static final long serialVersionUID = 1L;
	private String tipoDeLogradouro;
	private String logradouro;
	private String bairro;
	private String cidade;
	private String estado;
	private String cep;
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getTipo() {
		return tipoDeLogradouro;
	}
	public void setTipo(String tipo) {
		this.tipoDeLogradouro = tipo;
	}
	
}
