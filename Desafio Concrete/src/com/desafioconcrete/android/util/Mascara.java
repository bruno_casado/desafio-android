package com.desafioconcrete.android.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class Mascara {

	public static String unmask(String s){
		return s.replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[/]", "").replaceAll("[(]", "")
				.replaceAll("[)]", "");
	}
	
	public static TextWatcher insert(final String mask, final EditText editText){
		return new TextWatcher() {
			
			boolean isUpdating;
			String old = "";
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String str = Mascara.unmask(s.toString());
				String mascara = "";
				if (isUpdating){
					old = str;
					isUpdating = false;
					return;
				}
				
				int i = 0;
				for (char m : mask.toCharArray()){
					if (m != '#' && str.length() > old.length()){
						mascara += m;
						continue;
					}
					try{
						mascara += str.charAt(i);
					} catch (Exception e){
						break;
					}
					i++;
				}
				isUpdating = true;
				editText.setText(mascara);
				editText.setSelection(mascara.length());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		};
	}
}
